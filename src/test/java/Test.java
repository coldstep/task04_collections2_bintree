import com.bintree.CustomTree;
import com.bintree.Node;
import java.util.Set;

public class Test {

  public static void main(String[] args) {


    CustomTree<Integer,String> customTree = new CustomTree();


    customTree.put(6,"Rostik");
    customTree.put(1,"Vova");
    customTree.put(3,"Vitalik");

    customTree.put(5,"Rostik");
    customTree.put(7,"Vova");
    customTree.put(9,"Vitalik");
    customTree.put(12,"Rostik");
    customTree.put(-3,"Vova");
    customTree.put(2,"Vitalik");

    customTree.put(1,"Vova1");


    customTree.remove(3);
//
//    System.out.println(customTree.containsKey(1));

    Node<?,?> node = customTree.getRoot();
//
//    System.out.println(node.getKey());
//    System.out.println(node.getValue());

    System.out.println(node.toString());

//    System.out.println("Left leaf key - "+ node.getLeftNode().getKey());
//    System.out.println("Left leaf value - "+node.getLeftNode().getValue().toString());
//
//    System.out.println("Right leaf key - "+ node.getRightNode().getKey());
//    System.out.println("Right leaf value - "+ node.getRightNode().getValue().toString());


//
//    Set set = customTree.keySet();
//    for (Object o : set){
//      System.out.println(customTree.get(o));
//    }

  }

}
