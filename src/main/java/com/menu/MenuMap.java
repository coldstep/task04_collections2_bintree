package com.menu;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MenuMap {

  Map map = new HashMap();
  Scanner scanner = new Scanner(System.in);

  public void start() {
    System.out.println("    Welcome    ");
    startMenu();

  }

  private void startMenu() {
    Map menuMap = new HashMap();
    Map<String, Printable> functionMap = new HashMap();

    menuMap.put("1", "1. Create tree");
    menuMap.put("2", "2. Exit");

    functionMap.put("1", this::generateMap);
    functionMap.put("2", this::quit);

    for (Object o : menuMap.values()) {
      System.out.println(o);
    }

    while (!scanner.equals("2")) {
      functionMap.get(scanner.next()).generate();
    }


  }


  private void generateMap() {

    Map customMap1 = new HashMap();
    Map<String, Printable> customMap2 = new HashMap();

    customMap1.put("1", "1. Fill in map");
    customMap1.put("2", "2. Show map");
    customMap1.put("3", "3. Exit");

    customMap2.put("1", this::put);
    customMap2.put("2", this::show);
    customMap2.put("3", this::quit);

    while (!scanner.equals("3")) {
      for (Object o : customMap1.values()) {
        System.out.println(o);
      }
      customMap2.get(scanner.next()).generate();
    }

  }


  private void put() {
    String temp1 = "";
    String temp2 = "";
    boolean check = true;
    System.out.println("Write keys and then values when you finish write 'finish'!");

    do {
      System.out.println("Write key");
      temp1 = scanner.next();
      System.out.println("Write value");
      temp2 = scanner.next();

      if (temp1.equals("finish") == true || temp2.equals("finish") == true) {
        check = false;
      } else {
        map.put(temp1, temp2);
      }

    } while (check);
  }

  private void show() {
    for (Object obj : map.keySet()) {
      System.out.println("Key - " + obj + " : " + map.get(obj));
    }
  }


  private void quit() {
    System.exit(0);
  }

}
