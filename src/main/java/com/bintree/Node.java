package com.bintree;

public class Node<K extends Comparable, V> implements Comparable<Node<K, V>> {

  private K key;
  private V value;
  private Node leftNode;
  private Node rightNode;



  public Node(Object key, Object value) {
    this.key = (K) key;
    this.value = (V) value;
  }

  public K getKey() {
    return key;
  }

  public void setKey(K key) {
    this.key = key;
  }

  public V getValue() {
    return value;
  }

  public void setValue(V value) {
    this.value = value;
  }

  public Node getLeftNode() {
    return leftNode;
  }

  public void setLeftNode(Node leftNode) {
    this.leftNode = leftNode;
  }

  public Node getRightNode() {
    return rightNode;
  }

  public void setRightNode(Node rightNode) {
    this.rightNode = rightNode;
  }


  @Override
  public String toString() {
    return "Node {" +
        "\n\n key= " + key +
        ",\n value=" + value +
        ",\n\n leftNode=" + leftNode +
        ",\n\n rightNode=" + rightNode +
        "\n" + '}';
  }

  public int compareTo(Node<K, V> o) {
    return key.compareTo(o.getKey());
  }
}
