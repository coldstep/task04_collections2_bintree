package com.bintree;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;


public class CustomTree<K extends Comparable, V> implements Map {

  private int size;
  private Node<K, V> root;
  private Set set = new TreeSet();


  public Node<K, V> getRoot() {
    return root;
  }

  public CustomTree() {

  }

  public int size() {
    return size;
  }

  public boolean isEmpty() {
    return root == null;
  }

  public boolean containsKey(Object key) {
    Node current = root;
    Node temp = new Node(key, null);
    while (current.compareTo(temp) != 0) {
      if (current.compareTo(temp) < 0) {
        current = current.getRightNode();
      } else if (current.compareTo(temp) > 0) {
        current = current.getLeftNode();
      } else if (current == null) {
        return false;
      }
    }
    return true;
  }

  public boolean containsValue(Object value) {
    return true;

  }

  public Object get(Object key) {
    Node current = root;
    Node temp = new Node(key, null);
    while (current.compareTo(temp) != 0) {
      if (current.compareTo(temp) < 0) {
        current = current.getRightNode();
      } else if (current.compareTo(temp) > 0) {
        current = current.getLeftNode();
      } else if (current == null) {
        return null;
      }
    }
    return current.getValue();
  }

  public Object put(Object key, Object value) {
    Node current;
    Node previousParent = null;
    Node temp;
    boolean checker;

    checker = true;
    temp = new Node<K, V>(key, value);
    size++;

    if (root == null) {
      root = temp;
    } else {
      current = root;
      while (checker) {

        if (current.compareTo(temp) > 0) {
          if (current.getLeftNode() == null) {
            current.setLeftNode(temp);
            checker = false;
          } else {
            previousParent = current;
            current = current.getLeftNode();
          }
        } else if (current.compareTo(temp) < 0) {

          if (current.getRightNode() == null) {

            current.setRightNode(temp);
            checker = false;

          } else {
            previousParent = current;
            current = current.getRightNode();

          }

        } else if (current.compareTo(temp) == 0) {

          temp.setLeftNode(current.getLeftNode());
          temp.setRightNode(current.getRightNode());

          checker = false;
          if (previousParent.compareTo(temp) > 0) {
            previousParent.setLeftNode(temp);
          } else {
            previousParent.setRightNode(temp);
          }
          size--;
        }


      }
    }

    return null;
  }

  public Object remove(Object key) {
    Node current = root;
    Node previousParent = null;
    Node temp = new Node(key, null);

    while (current.compareTo(temp) != 0) {

      if (current.compareTo(temp) < 0) {
        previousParent = current;
        current = current.getRightNode();
      } else if (current.compareTo(temp) > 0) {
        previousParent = current;
        current = current.getLeftNode();
      } else if (current == null) {
        return null;
      }
    }

    if (current.getRightNode() == null && current.getLeftNode() == null) {

      if (previousParent.getLeftNode() == current) {
        previousParent.setLeftNode(null);
      } else {
        previousParent.setRightNode(null);
      }

    } else if (current.getRightNode() != null && current.getLeftNode() != null) {
      // not ready
      return null;
    } else if (current.getRightNode() != null) {
      if (previousParent.getLeftNode() == current) {
        previousParent.setLeftNode(current.getRightNode());
      } else {
        previousParent.setRightNode(current.getRightNode());
      }
    } else if (current.getLeftNode() != null) {
      if (previousParent.getLeftNode() == current) {
        previousParent.setLeftNode(current.getLeftNode());
      } else {
        previousParent.setRightNode(current.getLeftNode());
      }
    }
    return null;
  }

  public void putAll(Map m) {

  }

  public void clear() {
    size = 0;
    root = null;
  }

  public Set keySet() {
    checkBranches(root);
    return set;
  }

  public Collection values() {
    return null;
  }

  public Set<Entry> entrySet() {
    return null;
  }

  private void checkBranches(Node node) {
    if (node != null) {
      set.add(node.getKey());
    }
    if (node != null) {
      checkBranches(node.getLeftNode());
      checkBranches(node.getRightNode());
    }
  }
}
